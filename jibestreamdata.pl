use strict;
use DBI;

my $delim = '|';
my $mallpath = 'C:\Users\jacob.leon\JS_Mall_Files';	
	
`erase "$mallpath\\*" /Q`;		
	
my $dbh = DBI->connect('dbi:ODBC:Driver={SQL Server};Server={3S34B\SQLEXPRESS};database=prop_inv;Trusted_Connection=yes;{ChompBlanks=>1}')
    or die "Can't connect to DB: $DBI::errstr";
my $dbh_dets = DBI->connect('dbi:ODBC:Driver={SQL Server};Server={3S34B\SQLEXPRESS};database=prop_inv;Trusted_Connection=yes;{ChompBlanks=>1}')
    or die "Can't connect to DB: $DBI::errstr";	
	
my $mallqry = $dbh->prepare(
								q/Select  distinct
										p.PropertyID,
										PropertyName,
										State,
										City	
								  from dbo.PropertyObject oobj
								inner join dbo.Address a
								  on oobj.ObjectID = a.ObjectID
								inner join dbo.Property p
								  on oobj.PropertyID = p.PropertyID
								inner join dbo.Tenant t
								  on p.PropertyID = t.PropertyID
								where p.PropertyType = 'Mall'
								  and p.PropertyName <> ''/
					);	
	

$mallqry->execute();	
my @row;
my @malldata;

while (@malldata = $mallqry->fetchrow_array())
{			
			my $mallid    = $malldata[0];
			my $mallname  = $malldata[1];
			my $mallstate = $malldata[2];
			my $mallcity  = $malldata[3];
			
			
			my $filename = $mallid & '_' & $mallstate & '_' & $mallcity & '_' & $mallname & '.csv';
			my $filename = "$mallid\_$mallstate\_$mallcity\_$mallname\.csv";
	
	
			my $malldetsqry = $dbh_dets->prepare(
				q/Select 
						rtrim(JDE_TenantName) as TenantName,
						jde.UNIT_ID as UnitNumber,
						jde.UNIT_TYPE as UnitType,
						coalesce(prop.Category,'Undefined') as Category,
					    JDE_PropertyName as PropertyName,
						jde.master_lease_id as MasterLeaseID
				  from (                                   
									  Select distinct              
												PropertyName as PINV_PropertyName,       
												t.TenantName as PINV_TenantName, 
												suiteno,   
												p.PropertyID,
												max(tc.TenantCategory)  over (partition by PropertyName, t.TenantName) as Category       
										 from dbo.Property p        
								   inner JOIN dbo.Tenant t             
										   ON p.PropertyID = t.PropertyID
								   inner join dbo.TenantObject tob
										   on tob.TenantID = t.TenantID
								   inner join dbo.TenantCategory tc
										   on tob.TenantCategoryID = tc.TenantCategoryID
									  where 1=1                          
									  ) prop        
				RIGHT JOIN                                   
									  (Select distinct             
													MCDL01 as JDE_PropertyName,
													Customer_NM as JDE_TenantName,
													unit_id,
													building_id,
													unit_type,
													master_lease_id
										from dbo.JDE_Tenants              
										where 1=1                       
								--		  and LEASE_STATUS_DSC in ('Open','Holdover','Month-to-Month')
								--		  and lease_type_dsc in ('Retail','Specialty 365+','Department Store')
								--		  and LEASE_START_DT <= getDate()
								--		  and LEASE_MOVE_OUT is null
										  and UNIT_ID not like 'ATM%'
										 ) jde            
							  ON                    
									  replace(replace(lower(prop.PropertyID),' ',''),'.','') = replace(replace(lower(jde.BUILDING_ID),' ',''),'.','')              
							 and replace(replace(lower(prop.PINV_TenantName),' ',''),'.','') = replace(replace(lower(jde.JDE_TenantName),' ',''),'.','')   
						   where jde.building_id = ?
				/)
				or die "Error!!!! " . $dbh_dets->errstr;

				$malldetsqry->bind_param(1,$mallid);
				
				$malldetsqry->execute();
				my $TenantName =  $malldetsqry->{NAME}[0];
				my $UnitNumber =  $malldetsqry->{NAME}[1];
				my $UnitType =  $malldetsqry->{NAME}[2];
				my $Category = $malldetsqry->{NAME}[3];
				my $masterleaseid = $malldetsqry->{NAME}[5];
				my $Keywords = 'Keywords';
				
				open(my $file,'>>', "$mallpath/$filename");
				print $file "$masterleaseid$delim$UnitNumber$delim$TenantName$delim$UnitType$delim$Category$delim$Keywords\n";
				
				my @data;
				while(@data = $malldetsqry->fetchrow_array()){		
						my $masterlease = $data[5];
						my $tenant = $data[0];
						my $category = $data[3];
						my $unitid = $data[1];
						my $unittype = $data[2];
						
						my $line = "$masterlease$delim$unitid$delim$tenant$delim$unittype$delim$category$delim$category\n";		
						print $file $line;
				}
}

$dbh->disconnect;
$dbh_dets->disconnect;


